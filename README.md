"He de expresar que este proyecto es una primera alpha de una representación que nos hace pensar que podríamos vivir de otras maneras" Y nace desde las ganas de experimentar y entender un poquito más el mundo en el que vivimos.

# NeoCalendario

Calendario científico para un mundo más justo y con menos opresiones 

![imagen de las 4 estaciones en columnas de 3 meses separadas por días libres](año-cero.png)

## Prioridades
1. horas, semanas, meses con la misma base; regular.
2. semanas con mejor proporción descanso/trabajo
3. facilitar las festivitades paganas de cambio de estación
4. alineación a periodos astronómicos-naturales
    1. alinear los años a equinoccios (~360 días, base 6)
    2. ~~alinear los meses a las lunas (28 días, base 7)~~
5. estaciones del norte aunque intentamos que sea adaptable a trópicos y hemisferio sur
6. poder expresar fechas de civilizaciones antiguas con años "positivos", borrando así una barrera psicológica arbitraria entre lo "antes de cristo" y "después de cristo".
7. facilitar la conversión con el calendario gregoriano (también extendido hacia antes de que se inventara)

---

## Descripción

### Día

Igual que un día Gregoriano. 24 horas siempre iguales, que no dependen de la salida o puesta de sol.
Horas y minutos y segundos del sistema internacional de medidas.

Algunos días quedan fuera de calendario, sin ser contados forzando así a ignorarlos en términos de trabajo productivo. Se detalla en "estaciones".

### Semana

de 6 días

Los días de la semana toman estos nombres o derivados de éstos:

- Luna (festivo)
- Fuego
- Agua
- Aire
- Tierra
- Sol (festivo)

Nos hemos inspirado en la etimología latina de los días (luna, sol) y de la japonesa (fuego, agua, tierra). En vez de los 5 elementos tradicionales chinos, hemos usado los dos cuerpos celestes que nos acompañan con más influencia y los 4 elementos clásicos griegos, usando el orden de la semana japonesa en los elementos coincidentes.

Todas las semanas siempre empiezan y acaban igual, no hay semanas truncadas.

### Mes

propiedades:

- 5 semanas exactas por mes o 30 días exactos por mes sin excepción.
- Cada mes empieza el día 1 que es una luna/lunes.

Los meses los nombramos por su orden en la estación (ej: 2º de verano) o si es necesario, por el año (5º del año).

Los días del mes se pueden numerar con base decimal, docenal, o más útilmente, sexasimal, que coincide con la duración de las semanas. Esto hace que el primer dígito de un día del mes (ej: 24) represente la semana en la que está (segunda semana del mes) y el segundo dígito el día de la semana (del 0 al 5 por orden, en el ejemplo: tierra). Consideramos que es el más natural y amable para el habla, ya que no esconden cálculos que demandan tecnología (papel y lápiz) o una mente entrenada y con energía.

### Estación
Las estaciones calendarias son de 3 meses exactos, y se separan por días libres. Esta peculiaridad hace que sea posible compaginar unos meses regulares con unos solsticios y equinoccios en días "redondos".

Las estaciones tienen duraciones diferentes entre ellas debido a la inclinación del eje de rotación (cita requerida). Así, los veranos del norte son más largos que los del sur, que son igual de cortos que los inviernos del norte.

Aquí una tabla con las duraciones de las estaciones redondeadas a un año juliano de 365,25 días:

| Nord | Sud | Duració  |
| ------ | ------ | --- |
| PRIMAVERA | TARDOR | 92 dies 20 hores |
| ESTIU | HIVERN | 93 dies 15 hores |
| TARDOR | PRIMAVERA | 89 dies 19 hores |
| HIVERN | ESTIU | 89 dies 0 hores |

Proponemos los términos "resurgimiento", "luz", "recogimiento" y "oscuridad" para las 4 estaciones del hemisferio norte.

### Los días libres

El final de la primavera e inicio del verano "san juan", disfruta de 2 días de celebración fuera de calendario. Creamos así un vacío entre etapas anuales donde prima más el ritual, la contemplacion, la celebración y el encuentro popular, que no la ordenación de todas las cosas imaginables del mundo. Objetivo que persigue cualquier sistema con pretensiones autoriarias (la ciencia más patriarcal, las empresas capitalistas, los estados, o corporaciones para-estatales como Google). 

Como se puede observar en la tabla anterior, la estación trimestral, de 90 días exactos, no existe astronómicamente. Las largas (93 días) se pueden compensar parcialmente con días de traspaso, pero las dos estaciones cortas (89 días), no.

Como queremos mantener estaciones calendarias de 90 días, a otoño e invierno (del hemisferio norte) les damos un día de más. Esto compensa parcialmente que le dediquemos sólo 2 días al verano (faltaría casi 1 más para llegar a 93) y 2 días al otoño (1 día y medio más). Así, podemos dedicar todavía 1 día libre al final del año. Es también al final del año donde dedicaremos 1 día mas en los años bisiestos

### Año

Un año se compone de 360 días calendarizados que facilitan los cálculos. A su vez, cuenta con una duración media de 365,2425 días astronómicos de media, ya que nos adherimos de forma sincronizada con el calendario gregoriano a los ajustes de años bisiestos. En concreto:
- 1 de cada 4 años dispone de 1 día libre más, que se incorpora al fin de la oscuridad.
- 1 de cada 100 años no es bisiesto.
- 1 de cada 400 (o 1 de cada 4 de los de 100) sí que es bisiesto.

Numeración-nomenclatura:

- La numeración de los años es regular e incremental y no cambian con cambios de trono real o imperial.
- Nuestro año de referencia es el año 0, a diferencia del 1 AD gregoriano.
- Los años anteriores al 0 se representan como negativos: el año -1 es el justo anterior al 0.
- Los años posteriores al 0 se representan como positivos: el año 1 o +1 es el justo posterior al año 0.

### Siglo

Los siglos son agrupaciones de 100 años, útiles en la base decimal. Ésta y cualquier agrupación de años de base al quadrado, equivalente en otras bases (144 en sistemas 12), todas empiezan como los años por el 0.

Se puede calcular el siglo de un año con la función informática `floor( año / 100 )`.

Así, el año 0,1, 89, se encuentran en el siglo 0, y convenientemente, el año 6220 está en el siglo 62.

En cambio, en años antes de cero: el siglo -1 contiene los años -1, -89, etc. y el año -6220 se encuentra en el siglo -63.

## Compatibilidad con el calendario gregoriano

Aunque parezca lo contrario, la traducción entre calendarios es sencilla por dos motivos:
- seguimos el sistema de años bisiestos que dan de media años de 365,2425 días.
- escogemos un año gregoriano de referencia "redondo" en sistema decimal, para facilitar las sumas y restas.

## Repercusiones políticas

Consideramos que nuestro calendario no es "pre-científico" o "primitivista". Es una mejora post-vaticana, post-capitalista y que atorga más libertad (para vivir) y comodidades (para contar) al pueblo en vez de quitárselas y vigilarlas.

El hecho que marquemos días festivos (lunes y solem) se trata de una condición transitoria, reformista dentro del capitalismo y su trabajo asalariado (más días de descanso). Pero es fácilmente modificable a medida que se vaya superando y el trabajo no sea más que esfuerzo para cubrir necesidades individuales y colectivas.

Los espacios dejados expresamente entre estaciones promueven la reconexión con los ciclos vitales y climáticos, aunque éstos segundos estén cambiando rápidamente. Dificultan los cálculos productivistas y aceleracionistas donde cada hora no es más que un recurso a explotar y los meses nada más que etiquetas útiles en los libro de cuentas.

Como ya hemos indicado en la sección de limitaciones, no pretende ser un calendario universal, sino uno útil y adaptable, y con un poco de ánimo para definir con exactitud, interoperable.

Más aún, el simple atrevimiento de retar las divisones temporales del Imperio es liberador. Poder discernir qué es arbitrario de qué es justificable por la astronomía y con qué valores o prioridades se afrentan problemas sin solución exacta, nos ayuda a conocer mejor nuestro sistema cultural y las opresiones que canaliza. De igual forma que los mapas "políticos" dibujan límites imaginarios entre estados enemigos pero cómplices a la vez, los calendarios dibujan ciclos y fronteras imaginarios en el tiempo, que enfrentan pasado y futuro sin descanso.

Así, donde en los mapas se encuentran errores geográficos, fronteras disputadas, espacios reclamados pero no controlados y sitios secretos o superpuestos, el calendario gregoriano sólo consiguió disfrazar los ritos y ciclos paganos a las estaciones de la vida en la tierra salvaje o cultivada, el culto a la luna, los periodos anuales desfasados como los "cursos" que terminan en verano, etc. Hasta hubo 10 días de calendario que se borraron de un octubre en el siglo XVI. Esta propuesta de calendario abre explícitamente estas interzonas, no-tiempos, o días sin numeración, para evidenciar la arbitrariedad impuesta, para invitar a romper el orden y disponer de hiatos de más libertad, conciencia y amor.

Nos reconforta que el hecho de reconocer como legítimos estos residuos de la división del año, nos permite disfrutar el resto del año de periodos más racionales y ordenados que el machembrado del gregoriano. Es decir, admitir un poco de desorden nos permite tratar con mucha más agilidad matemática la mayoría del año.

Con este calendario esperamos ilusionaros tanto o más como a nosotrxs mismxs lo ha hecho el poder redefinir las divisiones temporales según nuestros deseos vitales.

---

## Hemos tenido en cuenta
Queremos tener en cuenta a la hora de hacer este calendario al menos los siguientes criterios:
- Ciencia, (astronomía, física, matemáticas)
- Influencias solares y lunares.
- Historia*.
- 

* En cuanto a Historia, queremos dar como año cero el inicio de la historia, que marcamos de momento en el 4199 antes de cristo como año cero por preceder los sistemas de escritura conocidos más antiguos.

-----

## Puntos fuertes

* soluciona molts problemes de comptar: "En quin dia de la setmana cau el proper 2 de febrer?". En el nostre calendari sempre cau en el mateix dia.
* determina uns festius molt xulos.
* ajuda a prendre més prespectiva històrica, lo qual és revolucionari per se
* els dies "extracalendaris" "d'equinoccis", "solsticis", de "celebració" o "de traspàs".


## y qué onda con las fases lunares?

Fases lunares no coinciden con los años ni con los meses. Creemos que no es posible hacerlo, de todas formas. El ciclo de la luna no es múltiplo de 24 horas, y estamos haciendo un calendario con "días".

Pero celebramos los años perfectos que es que coinciden el equinoccio de primavera con la luna llena, cada unos 30 años aprox (cita requerida).

El rollo es que las fases de la luna son muy irregulares
Duran entre 27 y 29,5 dias
Y la media (ponderada) si que son unos 28 dias

Miramos de hacer que los meses tuvieran 28 dias (4 semanas de 7), pero se nos iba mucho

Como que la luna no cierra el ciclo con los dias-noches, era demasiado dificil. Y teniamos que renegar de las estaciones y el año nuevo igualmente


## Limitaciones y sesgos

### Ajustes gregorianos

La diferencia entre la duración astronómica del año y la media de calendario, nos lleva a un error relevante cada 3000 años. Al mismo tiempo, los ciclos astronómicos tampoco son tales, ya que las velocidades de rotación y las distancias entre cuerpos celestes se van ralentizando y acortando. Compartimos estas limitaciones

### Culturas según clima

Este calendario está pensado para zonas climáticas con tradiciones festivas en primavera y otoño. En zonas equatoriales y tropicales, puede que las estaciones climáticas no empiecen y acaben con las "estaciones astronómicas".

Dadas nuestras limitaciones, si el sistema general del calendario puede ser útil, animamos a las personas con más conocimiento a derivar un calendario compatible con éste a partir de recolocar los días libres y por lo tanto de agrupar los meses. Para la comunicación entre zonas climáticas será esencial especificar los nombres de meses en términos inambiguos (2º mes de la estación seca) y declarar el calendario climático que sigue.

Desconocemos las celebraciones de los climas tropicales monzónicos y cualquier otro no representado por la cultura euro-cristiana.  Animamos de igual forma a todo el mundo a repensar el calendario y hacérselo suyo.

### Calendarios hermanos y compatibles

Las características que tiene que tener un calendario para considerarse hermano de éste:
- semanas de 6 días
- meses de 4 semanas
- año de 12 meses
- días libres de ajuste según duración gregoriana

Y para considerarse compatible deberá cumplir también:
- inicio del año en equinoccio de primavera.

Dicho de otra forma, dejamos a discreción de los pueblos que no vean representados sus ciclos naturales en este modelo, la reagrupación de meses y distribución de días de celebración según sus necesidades y deseos.

### Otros calendarios históricos

Sólo hemos tenido en cuenta los calendarios juliano y gregoriano europeos, junto con los datos astronómicos modernos que conocemos.

Hemos pasado por alto el estudio de calendarios lunares como el que se usa en el Islam, y de calendarios mixtos como el maya.
